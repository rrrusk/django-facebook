from django.conf.urls import patterns, url,include
from main import views
import social_auth

urlpatterns = patterns('',
    url(r'^$', views.main),
    url(r'contribute/', views.contribute),
    url(r'logout/', views.logout_view),
		url(r'user/(?P<id>\d+)/$', views.user),
    url(r'', include('social_auth.urls')),
)

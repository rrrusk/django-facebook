# -*- coding: utf-8 -*-
from django.template import RequestContext
from django.shortcuts import render, render_to_response, redirect
from main.models import Contribution
from django.contrib.auth.models import User
from django.contrib.auth import logout

def main(request):
	contribution = Contribution.objects.all()
	print contribution
	context = {"contribution":contribution,"user":request.user}
	return render_to_response('main/index.html',context)
# Create your views here.

def user(request,id):
	print id
	print User.objects.get(id=id)
	contribution = Contribution.objects.filter(user_id=id)
	context = {"contribution":contribution,"myuser":request.user,"user":User.objects.get(id=id)}
	return render_to_response('main/user.html',context)

def logout_view(request):
	logout(request)
	return render_to_response("main/logout.html",RequestContext(request))

def contribute(request):
	if request.method == "POST": #requestがPOSTかGETか判別
		u = User.objects.get(id=request.user.id)
		c = Contribution()
		c.user = u
		c.title = request.POST["title"]
		c.body = request.POST["body"]
		c.save()
		return redirect("/")
	else:
		return render_to_response('main/contribute.html',RequestContext(request))
